package main

import (
	"net/http"
	"fmt"
	"github.com/Shopify/sarama"
)

func makeMessageHandler(consumer sarama.PartitionConsumer) http.HandlerFunc {

	return func(resp http.ResponseWriter, req *http.Request) {
		resp.Header().Add("Content-Type", "application/json")
		select {
		case err := <-consumer.Errors():
			resp.Write([]byte(err.Error()))
			resp.WriteHeader(http.StatusNotFound)
		case msg := <-consumer.Messages():
			resp.Write([]byte(fmt.Sprintf("%s", string(msg.Value))))
		default:
			resp.Write([]byte(fmt.Sprintf("{%q: %q}", "status","no message available")))
		}
	}
}

func setupApi(server *http.ServeMux, consumer sarama.PartitionConsumer) {
	server.HandleFunc("/api/messages", makeMessageHandler(consumer))
}